import cv2
import glob
import os

if not os.path.exists("blue_channel_images/"):
    os.makedirs("blue_channel_images/")
if not os.path.exists("sobel_x_gen/"):
    os.makedirs("sobel_x_gen/")
if not os.path.exists("sobel_y_gen/"):
    os.makedirs("sobel_y_gen/")
if not os.path.exists("sobel_xy_gen/"):
    os.makedirs("sobel_xy_gen/")
if not os.path.exists("canny_edges/"):
    os.makedirs("canny_edges/")


def load_images(folder):
    for img in os.listdir(folder):
        img_name = img.split(os.path.sep)[0]
        if ".png" in img_name:
            img_name = img_name.strip(".png").strip()
        elif ".jpg" in img_name:
            img_name = img_name.strip(".jpg").strip()
        img = cv2.imread(os.path.join(folder, img))
        # # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # # img = cv2.resize(img, (img_dims[0], img_dims[1]))
        blackAndWhiteimg = monochrome_gen(img, img_name)
        grad_x = sobel_x_gen(img, img_name)
        grad_y = sobel_y_gen(img, img_name)
        sobel_xy_gen(grad_x, grad_y, img_name)
        canny_images(img, img_name)
    blue_channel_image_gen()
    # return img


def monochrome_gen(img, img_name):
    if not os.path.exists("monochrome_images/"):
        os.makedirs("monochrome_images/")
    grayImage = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blackAndWhiteImage = cv2.adaptiveThreshold(
        grayImage, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2
    )
    cv2.imwrite("monochrome_images/" + img_name + "_monochrome.png", blackAndWhiteImage)

    return blackAndWhiteImage


def blue_channel_image_gen(folder):
    for img in os.listdir(folder):
        img_name = img.split(os.path.sep)[0]
        if ".png" in img_name:
            img_name = img_name.strip(".png").strip()
        elif ".jpg" in img_name:
            img_name = img_name.strip(".jpg").strip()
        img = cv2.imread(img)
        # img = cv2.resize(img,(128,128))
        img[:, :, 1] = 0
        img[:, :, 2] = 0
        cv2.imwrite("blue_channel_images/" + img_name + ".png", img)


def sobel_x_gen(img, img_name):
    img[:, :, 0] = 0
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    grad_x = cv2.Sobel(
        img, cv2.CV_16S, 1, 0, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT
    )
    cv2.imwrite("sobel_x_gen/" + img_name + ".png", grad_x)
    return grad_x


def sobel_y_gen(img, img_name):
    img[:, :, 0] = 0
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    grad_y = cv2.Sobel(
        img, cv2.CV_16S, 0, 1, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT
    )
    cv2.imwrite("sobel_y_gen/" + img_name + ".png", grad_y)
    return grad_y


def sobel_xy_gen(grad_x, grad_y, img_name):
    abs_grade_x = cv2.convertScaleAbs(grad_x)
    abs_grade_y = cv2.convertScaleAbs(grad_y)
    grad = cv2.addWeighted(abs_grade_x, 0.5, abs_grade_y, 0.5, 0)
    cv2.imwrite("sobel_xy_gen/" + img_name + ".png", grad)


def canny_images(img, img_name):
    img = cv2.Canny(img, 160, 216)
    cv2.imwrite("canny_edges/" + img_name + ".png", img)


# print(img.split(os.path.sep))
load_images()
# blue_channel_image_gen()
