import os
import shutil

if not os.path.exists("train_images/"):
    os.makedirs("train_images/")

if not os.path.exists("train_annotations"):
    os.makedirs("train_images/annotations")

if not os.path.exists("test_images/"):
    os.makedirs("test_images/")

if not os.path.exists("test_images/annotations"):
    os.makedirs("test_images/annotations")


def images_decoupler(folder_name, training_ratio, annotation_folder=None):
    training_folder = "train_images/"
    testing_folder = "test_images/"

    training_annotations = "test_images/annotations"
    testing_annotations = "test_images/annotations"

    training_file_names = []
    testing_file_names = []

    img_count = os.listdir(folder_name)
    trainnig_count = round((training_ratio / 100) * len(img_count))
    # testing_count = len(img_count)-trainnig_count

    train_img_counter = 0
    for img in os.listdir(folder_name):

        file_name = img.split(os.path.sep)[0]

        if ".xml" not in file_name and ".jpg" in file_name:
            if train_img_counter <= trainnig_count:
                shutil.copy(folder_name + "/" + file_name, training_folder + file_name)
                training_file_names.append(file_name.strip(".jpg").strip())
            elif train_img_counter > trainnig_count:
                shutil.copy(folder_name + "/" + file_name, testing_folder + file_name)
                testing_file_names.append(file_name.strip(".jpg").strip())
            train_img_counter = train_img_counter + 1

        else:
            if ".xml" in file_name:
                if (
                    file_name.strip(".xml").strip() in training_file_names
                    and file_name not in testing_file_names
                ):
                    shutil.copy(
                        folder_name + "/" + file_name,
                        training_annotations + "/" + file_name,
                    )
                elif (
                    file_name.strip(".xml").strip() in testing_file_names
                    and file_name not in training_file_names
                ):
                    shutil.copy(
                        folder_name + "/" + file_name,
                        testing_annotations + "/" + file_name,
                    )

    if annotation_folder:
        for annotation in os.listdir(annotation_folder):
            file_name = annotation.split(os.path.sep)[0]
            if ".xml" in file_name:
                if (
                    file_name.strip(".xml").strip() in training_file_names
                    and file_name not in testing_file_names
                ):
                    shutil.copy(
                        annotation_folder + "/" + file_name,
                        training_annotations + "/" + file_name,
                    )
                elif (
                    file_name.strip(".xml").strip() in testing_file_names
                    and file_name not in training_file_names
                ):
                    shutil.copy(
                        annotation_folder + "/" + file_name,
                        testing_annotations + "/" + file_name,
                    )
