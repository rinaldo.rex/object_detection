# filtered_object_detection

Just applying narrow filters for object detection! 📷

Object detection using a typical tensorflow model is done to achieve a generic 
detection. However, for narrow use cases, it's better if we change what image data to
 be processed. This increases the accuracy of the model by manyfold if applied 
 properly. An example of this kind of detection is by applying filters on the images 
 thereby promptly reducing the data that has to be processed. It makes the model 
 easier, faster, and more accurate for narrowed use cases. 
 
### NOTE 

To run the application, ensure you set the configurations properly as per the 
following. (This is the default environment!)

- `export ENV_FOR_DYNACONF=development` for running with dev values


#### Ensure you install the following.

- Install the dependencies with `pipenv install`
- Activate your virtual environment with `pipenv shell`
- Install **fcli** command line application with `pipenv install -e .`

This will install fcli as a command line application! 🎉🥳

You can use _fcli_ as any other command line application. You'll get in-app help 
documentations too! Have fun!
 
## CLI

### Example references for usage. 

- `fcli prep` - Prepares the model training environment. (Required installs, 
extractions,
 etc)

###### Applying filters on images
- `fcli filter --in-path='train_images/original' --out-path='train_images' --mode='all'`
- `fcli filter --in-path='test_images/original' --out-path='test_images' --mode='all'`

###### Processing labels/annotations for the images
(On training images)
- `fcli label --labels-path=train_images/annotations/ --xml-to-csv`
- `fcli label --labels-path=train_images/annotations/ --label-map`
- `fcli label --labels-path=train_images/annotations/ --config`

(On test images)
- `fcli label --labels-path=test_images/annotations/ --xml-to-csv`
- `fcli label --labels-path=test_images/annotations/ --label-map`
- `fcli label --labels-path=test_images/annotations/ --config`

###### Processing TF Records (Generating TensorFlow records)
- `fcli tfrecord --path='train_images/annotations/'`
- `fcli tfrecord --path='test_images/annotations/'`

###### Training required models
- `fcli train --model='original'` 
- `fcli train --model='canny'` 
- `fcli train --model='sobelx'` 
- `fcli train --model='sobely'` 
- `fcli train --model='sobelxy'` 
- `fcli train --model='m2b'` 

---
