"""
cli.py forms the wrapper for the other subcommands of the modules.
"""
import click
from click import secho
import logging

# Setting logging level to ERROR to suppress tensorflow warnings because we're importing
# Tensorflow below when importing commands from core package.
logging.getLogger("tensorflow").setLevel(logging.ERROR)

import sys

# Append path here so the following import of object detection from core is applicable
# Appending here because we're importing slim and object detection below
sys.path.append("./object_detection")
sys.path.append("./slim")

from core.commands import gray, process, filter_, label, train, prepare, generate_tf


@click.group()
def cli():
    """
    Cli is the overall grouping command line application that performs all the other
    commands.

    This will also help grouping of logical commands.

    # TODO: rename this is making this package available for install via setup.py
    :return:
    """
    pass


@cli.command("test")
def test_run():
    """
    Just a test for the cli to check if it's working properly.

    :return:
    """

    secho("Well hello there, I'm feeling good!", blink=True, fg="green")


# Include all the commands as part of the cli
cli.add_command(gray)
cli.add_command(process)
cli.add_command(filter_)
cli.add_command(label)
cli.add_command(train)
cli.add_command(prepare)
cli.add_command(generate_tf)


if __name__ == "__main__":
    cli()
