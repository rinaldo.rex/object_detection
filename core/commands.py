"""
All the commands as part of the group CLI (as of now) are
"""

import click
import os
import csv
import cv2

from click import secho
from dynaconf import settings
from pathlib import Path
from core.utils.image_filters import *
from core.utils.xml_to_csv import generate_csv
from core.utils.generate_tfrecord import generate_tfrecords
from core.utils.generate_label_map import generate_label_map
from core.utils.generate_train_config import generate_train_config
from core.utils.image_decoupler import decoupler
from core.utils.train import train_models
from shutil import unpack_archive, rmtree


@click.command()
@click.option("--make/--no-make", help="Make grayscale images!", default=False)
@click.option(
    "--in-path", help="Input path for images", default=settings.DEFAULT_IMAGE_INPUT_DIR
)
@click.option(
    "--out-path",
    help="Output path for processed images",
    default=settings.DEFAULT_IMAGE_OUTPUT_DIR,
)
def gray(in_path, out_path, make=False):
    """
    Process the input images and make grayscale versions of them.

    :return:
    """

    click.confirm(
        f"Using the path: '{in_path}' for input images "
        f"and '{out_path}' for output images. "
        " Proceed?",
        abort=True,
    )


@click.command()
@click.pass_context
def process(ctx, process=None):
    # print("calling other filters")
    ctx.invoke(gray)


@click.command("filter")
@click.option("--mode", default="all", help="The type of filter to apply on the images")
@click.option("--in-path", help="Input path for images")
@click.option("--out-path", help="Output path for processed images")
def filter_(mode, in_path, out_path):
    """
    Applies filter to the images based on the other parameters.

    mode:
        str: all/canny/m2b/sobelx/sobely/sobelxy

            all - Processes all the images by applying all the available filters.

            canny - Applies only canny filter to the images

            m2b - Applies the monochrome data onto the blue channel of the images

            sobelx - Applies sobel filter along x axis.

            sobely - Applies sobel filter along the y axis.

            sobelxy - Applies the sobel filter on both x and y axis of the image.

    in-path:
        str: Input directory containing all the original images.

    out-path:
        str: The output directory where processed images will be generated onto their
            own directories, respectively.

    :return:
    """
    out_path = Path(out_path)
    if mode == "all":
        all_flag = True
    else:
        all_flag = False

    for file in os.listdir(in_path):
        if file == ".gitkeep":
            # Skip gitkeep file from being processed. 🤦
            continue

        img_path = Path.joinpath(Path(in_path), file)
        img = cv2.imread(str(img_path))

        if mode == "canny" or all_flag:
            # Apply canny filter
            # Generate a new directory under out_path with canny applied images
            out_file_path = str(out_path.joinpath(f"canny/{file}"))
            if not Path.joinpath(out_path, "canny").exists():
                print(f"Path: {out_path} not existing. Creating")
                os.makedirs(Path.joinpath(out_path, "canny"))
            nimg = canny(img)
            cv2.imwrite(out_file_path, nimg)
        if mode == "m2b" or all_flag:
            # Apply monochrome data to blue channel
            out_file_path = str(out_path.joinpath(f"m2b/{file}"))
            if not Path.joinpath(out_path, "m2b").exists():
                print(f"Path: {out_path} not existing. Creating")
                os.makedirs(Path.joinpath(out_path, "m2b"))
            nimg = blue_channel(monochrome(img))
            cv2.imwrite(out_file_path, nimg)
        if mode == "sobelx" or all_flag:
            # Apply sobelx filter
            out_file_path = str(out_path.joinpath(f"sobelx/{file}"))
            if not Path.joinpath(out_path, "sobelx").exists():
                print(f"Path: {out_path} not existing. Creating")
                os.makedirs(Path.joinpath(out_path, "sobelx"))
            nimg = sobel_x(img)
            cv2.imwrite(out_file_path, nimg)
        if mode == "sobely" or all_flag:
            # Apply sobely filter
            out_file_path = str(out_path.joinpath(f"sobely/{file}"))
            if not Path.joinpath(out_path, "sobely").exists():
                print(f"Path: {out_path} not existing. Creating")
                os.makedirs(Path.joinpath(out_path, "sobely"))
            nimg = sobel_y(img)
            cv2.imwrite(out_file_path, nimg)
        if mode == "sobelxy" or all_flag:
            # Apply sobelxy filter
            out_file_path = str(out_path.joinpath(f"sobelxy/{file}"))
            if not Path.joinpath(out_path, "sobelxy").exists():
                print(f"Path: {out_path} not existing. Creating")
                os.makedirs(Path.joinpath(out_path, "sobelxy"))
            nimg = sobel_xy(img)
            cv2.imwrite(out_file_path, nimg)

    return True


@click.command("label")
@click.option("--xml-to-csv", is_flag=True, help="Generate CSV from XML annotations")
@click.option("--label-map", is_flag=True, help="Generate label map using TF-Records")
@click.option("--tf-record", is_flag=True, help="Generate TF-Record from CSV")
@click.option("--config", is_flag=True, help="Generate training config")
@click.option("--mode", help="The filter type")
@click.option("--labels-path", help="Path to annotations/CSV/tfrecord files")
def label(
    labels_path,
    xml_to_csv=False,
    tf_record=False,
    label_map=False,
    config=True,
    mode=None,
):
    """
    Command to handle label processing for model training

    labels-path: str: The path to the annotations files. (XML)

    xml-to-csv: (Flag) If applied, generates CSV files from XML annotations

    lable-map: (Flag) If applied, generates label map mapping object labels to indices.

    config: (Flag) If applied, generates training config with respective internal
    values.

    :return:
    """

    if xml_to_csv:
        print(f"Generating CSV from XML in {labels_path}")
        generate_csv(labels_path)

    print(f"Generating id-class mapping in {labels_path}")

    object_classes = set()
    # Open the lables.csv file to fetch the classes
    reader = csv.reader(open(labels_path + "labels.csv"))
    # with open(labels_path+'labels.csv') as csvfile:
    for row in reader:
        object_classes.add(row[3])

    if label_map:
        print(f"Generating label-map.pbtxt from labels.csv in {labels_path}")
        generate_label_map(
            label_map_path=settings.TRAINING_DIR, classes=list(object_classes)
        )
        secho(
            f"Successfully created label-map.pbtxt in {settings.TRAINING_DIR}",
            fg="green",
        )

    if config:
        for config_dir in ["original", "canny", "m2b", "sobelx", "sobely", "sobelxy"]:
            secho(f"Generating training config")
            generate_train_config(records_path=config_dir, classes=object_classes)

    if tf_record:
        print(f"Generating TFRecords from CSV in {labels_path}")
        image_dir = mode
        print(f"Generating tfrecord for : {image_dir}")
        generate_tfrecords(labels_path, image_dir, classes=list(object_classes))
        # click.confirm(abort=True)


@click.command("tfrecord")
@click.option("--path", help="")
def generate_tf(path):
    """
    Generate TF Records (Based on the CSV labels)

    :return:
    """

    if not path:
        labels_path = "test_images/annotations/"
    else:
        labels_path=path
    object_classes = set()
    # Open the lables.csv file to fetch the classes
    reader = csv.reader(open(labels_path + "labels.csv"))
    # with open(labels_path+'labels.csv') as csvfile:
    for row in reader:
        object_classes.add(row[3])

    print(f"Generating TFRecords from CSV in {path}")
    image_dirs = ["canny", "sobelx", "sobely", "sobelxy", "original", "m2b"]
    print(f"Generating tfrecord for : {image_dirs}")
    generate_tfrecords(labels_path, image_dirs, classes=list(object_classes))


@click.command("train")
@click.option("--train-dir", default=None, help="Training config and output dir")
@click.option("--train-config", help="Training config and output dir", default='training')
@click.option("--model", help="Which model to train")
def train(train_dir, train_config, model):
    """
    Handle model training

    train-dir: str: The path where the training data is stored.

    model: str: original/canny/sobelx/sobely/sobelxy/m2b

        Generates the particular model based on the filtered images type.
        Note that this takes a default training dirs. For custom training dir,
        use --train-dir

    train-dir: str: The training directory where the data is stored.

    :param train_dir:
    :return:
    """

    train_config_path = train_config

    if train_dir is None:
        if model == "original":
            train_dir = "training/original"
            train_config_path = "training/train_original.config"
        elif model == "canny":
            train_dir = "training/canny"
            train_config_path = "training/train_canny.config"
        elif model == "sobelx":
            train_dir = "training/sobelx"
            train_config_path = "training/train_sobelx.config"
        elif model == "sobely":
            train_dir = "training/sobely"
            train_config_path = "training/train_sobely.config"
        elif model == "sobelxy":
            train_dir = "training/sobelxy"
            train_config_path = "training/train_sobelxy.config"
        elif model == "m2b":
            train_dir = "training/m2b"
            train_config_path = "training/train_m2b.config"
        else:
            train_dir = "training"

    secho(
        f"Training models in dir: {train_dir} with config from: {str(train_config_path)}"
    )

    train_models(train_dir=str(train_dir), config_path=str(train_config_path))


@click.command("prep")
def prepare():
    """
    Prepare the environment.

    Do checks here such as dir permissions, dir creations, installations if any.
    :return:
    """
    secho("Preparing filter detector!", fg="yellow")
    secho("Installing slim for object_detection", fg="blue")

    from distutils.core import run_setup

    path_ = Path.joinpath(Path(os.getcwd()), "slim", "setup.py")
    run_setup(str(path_), ["build", "install"])
    secho("Successfully installed slim", fg="green")

    secho("Extracting pre-trained model", fg="blue")
    # Delete the dir if already exisiting
    if Path("training/model").exists():
        rmtree("training/model")

    unpack_archive(
        "training/faster_rcnn_inception_v2_coco_2018_01_28.tar.gz", "training/"
    )
    os.rename("training/faster_rcnn_inception_v2_coco_2018_01_28", "training/model")
    secho("Successfully extracted models", fg="green")

    secho(f"Decoupling images from dir: images", fg="blue")
    decoupler()
