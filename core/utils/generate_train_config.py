import tensorflow as tf
from google.protobuf import text_format
from object_detection.protos import pipeline_pb2
from pathlib import Path
import os


def generate_train_config(pipeline=None, output=None, records_path=None, classes=None):
    """
    Generates the training config file (based on google's protobuf) with the given
    parameters.

    :param pipeline:
    :param output:
    :param records_path:
    :param classes:
    :return:
    """
    pipeline_config = pipeline_pb2.TrainEvalPipelineConfig()
    if pipeline is None:
        pipeline = str(Path.joinpath(Path("core", "utils", "train_sample.config")))

    with tf.gfile.GFile(pipeline, "r") as f:
        proto_str = f.read()
        text_format.Merge(proto_str, pipeline_config)

    if records_path is None:
        records_path = "original"
    # Setting default paths
    CHECKPOINT_PATH = str(
        Path.joinpath(Path(os.getcwd()), "training", "model", "model.ckpt")
    )

    # Train paths
    TRAIN_RECORD_PATH = str(
        Path.joinpath(Path(os.getcwd()), "train_images", records_path, "labels.record")
    )
    TRAIN_LABEL_PATH = str(
        Path.joinpath(Path(os.getcwd()), "training", "label_map.pbtxt")
    )

    # Test paths
    TEST_RECORD_PATH = str(
        Path.joinpath(Path(os.getcwd()), "test_images", records_path, "labels.record")
    )
    TEST_LABEL_PATH = str(
        Path.joinpath(Path(os.getcwd()), "training", "label_map.pbtxt")
    )

    # No. of object classes
    OBJECT_CLASSES = int(len(classes))
    pipeline_config.train_config.fine_tune_checkpoint = CHECKPOINT_PATH

    pipeline_config.train_input_reader.tf_record_input_reader.input_path[
        0
    ] = TRAIN_RECORD_PATH
    pipeline_config.train_input_reader.label_map_path = TRAIN_LABEL_PATH

    pipeline_config.eval_input_reader[0].tf_record_input_reader.input_path[
        0
    ] = TEST_RECORD_PATH
    pipeline_config.eval_input_reader[0].label_map_path = TEST_LABEL_PATH

    pipeline_config.model.faster_rcnn.num_classes = OBJECT_CLASSES

    config_text = text_format.MessageToString(pipeline_config)

    # output = records_path
    if output is None:
        output = str(Path.joinpath(Path("training", f"train_{records_path}.config")))
    with tf.gfile.Open(output, "wb") as f:
        f.write(config_text)
