"""
Image decoupler splits the given images and their associated annotations into
train and test images + annotations.

"""
import os
import shutil


if not os.path.exists("train_images/original"):
    os.makedirs("train_images/original")

if not os.path.exists("train_images/annotations"):
    os.makedirs("train_images/annotations")

if not os.path.exists("test_images/original"):
    os.makedirs("test_images/original")

if not os.path.exists("test_images/annotations"):
    os.makedirs("test_images/annotations")

training_annotations = "train_images/annotations/"
testing_annotations = "test_images/annotations/"

training_folder = "train_images/original/"
testing_folder = "test_images/original/"


def images_decoupler(folder_name, training_ratio):
    """
    Splits the given directory of images into train and test data/images

    :param folder_name: The directory containing all the images.
    :param training_ratio: The ratio of train:test images
    :return:
    """

    training_file_names = []
    testing_file_names = []

    img_count = os.listdir(folder_name)
    img_count = [item for item in img_count if ".jpg" in item]
    trainnig_count = round((training_ratio / 100) * len(img_count))
    # testing_count = len(img_count)-trainnig_count

    train_img_counter = 0
    for img in os.listdir(folder_name):

        file_name = img.split(os.path.sep)[0]

        if file_name == ".gitkeep" or file_name == ".gitignore":
            continue

        if ".jpg" in file_name:
            if train_img_counter <= trainnig_count:
                shutil.copy(folder_name + "/" + file_name, training_folder + file_name)
                training_file_names.append(file_name.strip(".jpg").strip())
            elif (
                train_img_counter > trainnig_count
                and file_name.strip(".jpg").strip() not in training_file_names
            ):
                shutil.copy(folder_name + "/" + file_name, testing_folder + file_name)
                testing_file_names.append(file_name.strip(".jpg").strip())
            train_img_counter = train_img_counter + 1
    return training_file_names, testing_file_names


def xml_decoupler(folder_name, testing_file_names, training_file_names):
    """
    Splits the given annotations into train and test annotations.

    :param folder_name: The directory containing train and test annotations together.
    :param testing_file_names:
    :param training_file_names:
    :return:
    """
    for annotation in os.listdir(folder_name):
        file_name = annotation.split(os.path.sep)[0]
        if ".xml" in file_name:
            if file_name.strip(".xml").strip() in training_file_names:
                shutil.copy(
                    folder_name + "/" + file_name, training_annotations + file_name
                )
                # training_file_names.remove(file_name.strip('.xml').strip())

            elif file_name.strip(".xml").strip() in testing_file_names:
                shutil.copy(
                    folder_name + "/" + file_name, testing_annotations + file_name
                )
                # testing_file_names.remove(file_name.strip('.xml').strip())


def decoupler(folder_name="samples/images", trainning_ratio=80, annotation_file=None):
    """
    Function to split the images and annotations set into train and test dataset.

    :param folder_name: The directory containing the images (+ annotations)
    :param trainning_ratio: The train:test ratio of the dataset to split.
    :param annotation_file: The annotations file(optional)
    :return:
    """
    training_file_names, testing_file_names = images_decoupler(
        folder_name=folder_name, training_ratio=trainning_ratio
    )
    if annotation_file:
        xml_decoupler(
            folder_name=annotation_file,
            testing_file_names=testing_file_names,
            training_file_names=training_file_names,
        )
    else:
        xml_decoupler(
            folder_name=folder_name,
            testing_file_names=testing_file_names,
            training_file_names=training_file_names,
        )
