"""
Applicable image filters as utilities.
"""
import cv2
import numpy as np


def monochrome(img):
    """
    Apply monochrome filter on image

    :param img:
    :return:
        monochrome img
    """
    grayImage = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blackAndWhiteImage = cv2.adaptiveThreshold(
        grayImage, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2
    )
    img_1 = []
    for row in blackAndWhiteImage:
        row = cv2.resize(row, (3,1024))
        img_1.append(row)
    m_img = np.asarray(img_1)
    return m_img


def sobel_x(img):
    """
    Apply sobel on x axis of img

    :param img:
    :return:
    """
    img[:, :, 0] = 0
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobel_x_img = cv2.Sobel(
        img, cv2.CV_16S, 1, 0, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT
    )

    return sobel_x_img


def sobel_y(img):
    """
    Apply sobel on Y axis of img

    :param img:
    :return:
    """
    img[:, :, 0] = 0
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    sobel_y_img = cv2.Sobel(
        img, cv2.CV_16S, 0, 1, ksize=3, scale=1, delta=0, borderType=cv2.BORDER_DEFAULT
    )

    return sobel_y_img


def sobel_xy(img):
    """
    Apply sobel on both x and y axis of the img

    :param img:
    :return:
    """

    abs_grade_x = cv2.convertScaleAbs(sobel_x(img))
    abs_grade_y = cv2.convertScaleAbs(sobel_y(img))
    sobel_xy_img = cv2.addWeighted(abs_grade_x, 0.5, abs_grade_y, 0.5, 0)

    return sobel_xy_img


def blue_channel(img):
    """
    Apply only blue channel to the given image

    :param img:
    :return:
    """
    # Set G and R channel to 0
    img[:, :, 1] = 0
    img[:, :, 2] = 0

    return img


def canny(img):
    """
    Apply blurred canny filter to img

    :param img:
    :return:
    """
    canned = cv2.Canny(img, 160, 216)
    return canned
