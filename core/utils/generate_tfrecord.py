"""
Usage:
  # From tensorflow/models/
  # Create train data:
  python generate_tfrecord.py --csv_input=data/train_labels.csv  --output_path=train.record

  # Create test data:
  python generate_tfrecord.py --csv_input=data/test_labels.csv  --output_path=test.record
"""
from __future__ import division
from __future__ import print_function
from __future__ import absolute_import
from pathlib import Path
import os
import io
import pandas as pd
import tensorflow as tf

from PIL import Image
from object_detection.utils import dataset_util
from collections import namedtuple, OrderedDict

import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

flags = tf.app.flags
FLAGS = flags.FLAGS


def split(df, group):
    data = namedtuple("data", ["filename", "object"])
    gb = df.groupby(group)
    return [
        data(filename, gb.get_group(x))
        for filename, x in zip(gb.groups.keys(), gb.groups)
    ]


def create_tf_example(group, path):
    with tf.io.gfile.GFile(
        os.path.join(path, "{}".format(group.filename)), "rb"
    ) as fid:
        encoded_jpg = fid.read()
    encoded_jpg_io = io.BytesIO(encoded_jpg)
    image = Image.open(encoded_jpg_io)
    width, height = image.size

    filename = group.filename.encode("utf8")
    image_format = b"jpg"
    xmins = []
    xmaxs = []
    ymins = []
    ymaxs = []
    classes_text = []
    classes = []

    for index, row in group.object.iterrows():
        xmins.append(row["xmin"] / width)
        xmaxs.append(row["xmax"] / width)
        ymins.append(row["ymin"] / height)
        ymaxs.append(row["ymax"] / height)
        classes_text.append(row["class"].encode("utf8"))
        classes.append(FLAGS.classes.index(row["class"]))

    tf_example = tf.train.Example(
        features=tf.train.Features(
            feature={
                "image/height": dataset_util.int64_feature(height),
                "image/width": dataset_util.int64_feature(width),
                "image/filename": dataset_util.bytes_feature(filename),
                "image/source_id": dataset_util.bytes_feature(filename),
                "image/encoded": dataset_util.bytes_feature(encoded_jpg),
                "image/format": dataset_util.bytes_feature(image_format),
                "image/object/bbox/xmin": dataset_util.float_list_feature(xmins),
                "image/object/bbox/xmax": dataset_util.float_list_feature(xmaxs),
                "image/object/bbox/ymin": dataset_util.float_list_feature(ymins),
                "image/object/bbox/ymax": dataset_util.float_list_feature(ymaxs),
                "image/object/class/text": dataset_util.bytes_list_feature(
                    classes_text
                ),
                "image/object/class/label": dataset_util.int64_list_feature(classes),
            }
        )
    )
    return tf_example


def main(args):
    print("args ------------", args)
    labels_path = args[1]

    for image_dir in args[0]:
        output_path = Path.joinpath(
            Path(os.getcwd()), labels_path, "..", image_dir, "labels.record"
        )
        image_path = Path.joinpath(Path(os.getcwd()), labels_path, "..", image_dir)

        flags.DEFINE_string("output_path", str(output_path), "Path to output TFRecord")
        flags.DEFINE_string("image_dir", str(image_path), "Path to images")

        writer = tf.io.TFRecordWriter(FLAGS.output_path)
        path = os.path.join(FLAGS.image_dir)
        examples = pd.read_csv(FLAGS.csv_input)
        classes = FLAGS.classes
        grouped = split(examples, "filename")
        for group in grouped:
            tf_example = create_tf_example(group, path)
            writer.write(tf_example.SerializeToString())

        writer.close()
        output_path = os.path.join(os.getcwd(), FLAGS.output_path)
        print("Successfully created the TFRecords: {}".format(output_path))

        def del_all_flags(FLAGS):
            flags_dict = FLAGS._flags()
            keys_list = [keys for keys in flags_dict]
            for keys in keys_list:
                if keys == "output_path" or keys == "image_dir":
                    FLAGS.__delattr__(keys)

        del_all_flags(tf.flags.FLAGS)


def generate_tfrecords(labels_path, image_dirs, classes):
    csv_path = Path.joinpath(Path(os.getcwd()), labels_path, "labels.csv")
    flags.DEFINE_string("csv_input", str(csv_path), "Path to the CSV input")
    flags.DEFINE_list("classes", classes, "Path to images")

    tf.compat.v1.app.run(main, (image_dirs, labels_path))
