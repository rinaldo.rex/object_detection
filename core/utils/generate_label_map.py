from object_detection.protos.string_int_label_map_pb2 import (
    StringIntLabelMap,
    StringIntLabelMapItem,
)
from google.protobuf import text_format
from pathlib import Path


def convert_classes(classes, start=1):
    """
    Generate a id-object class mapping using google's protobuf.
    
    :param classes:
    :param start:
    :return:
    """
    msg = StringIntLabelMap()
    for id, name in enumerate(classes, start=start):
        msg.item.append(StringIntLabelMapItem(id=id, name=name))

    text = str(text_format.MessageToBytes(msg, as_utf8=True), "utf-8")
    return text


def generate_label_map(label_map_path, classes):
    """
    Generates label map from the given list of object classes

    :param label_map_path:
    :param classes:
    :return:
    """
    txt = convert_classes(classes)

    label_map_path = Path.joinpath(Path(label_map_path), "label_map.pbtxt")
    with open(str(label_map_path), "w") as f:
        f.write(txt)
